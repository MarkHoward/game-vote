#!/bin/sh

echo "Waiting for postgres..."

while ! nc -z db 5432; do
    sleep 0.1
done

echo "PostgreSQL started"

alembic upgrade head
# gunicorn --worker-class eventlet -w 1 vote.main:app --bind 0.0.0.0:8000
uvicorn vote.main:app --host=0.0.0.0 --port=8000

exec "$@"