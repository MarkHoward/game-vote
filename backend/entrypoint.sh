#!/bin/sh

echo "Waiting for postgres..."

while ! nc -z db 5432; do
    sleep 0.1
done

echo "PostgreSQL started"

alembic revision --autogenerate
alembic upgrade head
uvicorn vote.main:app --reload --host=0.0.0.0 --port=8000

exec "$@"