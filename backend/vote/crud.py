from sqlalchemy.orm import Session

from . import models, schemas


def get_game(db: Session, game_id: int):
    return db.query(models.Game).filter(models.Game.id == game_id).first()


def get_game_by_name(db: Session, game_name: str):
    return db.query(models.Game).filter(models.Game.name == game_name).first()


def get_games(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Game).offset(skip).limit(limit).all()


def create_game(db: Session, game: schemas.GameCreate):
    db_game = models.Game(name=game.name,
                          min_players=game.min_players,
                          max_players=game.max_players,
                          bgg_link=game.bgg_link,)
    db.add(db_game)
    db.commit()
    db.refresh(db_game)
    return db_game


def get_votes(db: Session, skip: int = 0, limit: int = 100):
    return db.query(models.Vote).offset(skip).limit(limit).all()


def create_vote(db: Session, vote: schemas.VoteCreate):
    db_vote = models.Vote(value=vote.value, game_id=vote.game_id)
    db.add(db_vote)
    db.commit()
    db.refresh(db_vote)
    return db_vote


def delete_votes(db: Session):
    db.query(models.Vote).delete()
    db.commit()
