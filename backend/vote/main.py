import json
from typing import List

from fastapi import Depends, FastAPI, HTTPException, WebSocket, WebSocketDisconnect
from pydantic import BaseModel
from sqlalchemy.orm import Session

from . import crud, models, schemas
from .database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()


class Status(BaseModel):
    message: str


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.post("/api/games", response_model=schemas.Game)
def create_game(game: schemas.GameCreate, db: Session = Depends(get_db)):
    db_game = crud.get_game_by_name(db=db, game_name=game.name)
    if db_game:
        raise HTTPException(status_code=400, detail="A game with this name already exists")
    return crud.create_game(db=db, game=game)


@app.get("/api/games", response_model=List[schemas.Game])
def list_games(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    return crud.get_games(db=db, skip=skip, limit=limit)


@app.get("/api/games/{game_id}", response_model=schemas.Game)
def retrieve_game(game_id: int, db: Session = Depends(get_db)):
    db_game = crud.get_game(db=db, game_id=game_id)
    if db_game is None:
        raise HTTPException(status_code=404, detail="Game not found")
    return db_game


@app.post("/api/votes", response_model=List[schemas.Vote])
def create_vote(votes: List[schemas.VoteCreate], db: Session = Depends(get_db)):
    return [crud.create_vote(db=db, vote=vote) for vote in votes]


@app.get("/api/votes", response_model=List[schemas.Vote])
def list_votes(skip: int = 0, limit: int = 100, db: Session = Depends(get_db)):
    return crud.get_votes(db=db, skip=skip, limit=limit)


@app.delete("/api/votes", response_model=Status)
def delete_votes(db: Session = Depends(get_db)):
    crud.delete_votes(db=db)
    return Status(message="Votes deleted")


class ConnectionManager:
    def __init__(self):
        self.active_connections: List[WebSocket] = []

    async def connect(self, websocket: WebSocket):
        await websocket.accept()
        self.active_connections.append(websocket)

    def disconnect(self, websocket: WebSocket):
        self.active_connections.remove(websocket)

    async def broadcast(self, message: str):
        for connection in self.active_connections:
            await connection.send_json(message)


manager = ConnectionManager()


@app.websocket("")
async def websocket_endpoint(websocket: WebSocket):
    await manager.connect(websocket)
    try:
        while True:
            pass
    except WebSocketDisconnect:
        manager.disconnect(websocket)


@app.get("/api/tally")
async def websocket_push(websocket: WebSocket, db: Session = Depends(get_db)):
    data = json.dumps(crud.get_games(db=db))
    data["event"] = "tally"
    manager.broadcast(data)
    return Status(message="Tally broadcasted")
