from sqlalchemy import Column, ForeignKey, Integer, String
from sqlalchemy.orm import relationship

from .database import Base


class Game(Base):
    __tablename__ = "games"

    id = Column(Integer, primary_key=True, index=True)
    name = Column(String, unique=True, index=True)
    min_players = Column(Integer)
    max_players = Column(Integer)
    bgg_link = Column(String, unique=True)

    votes = relationship("Vote", back_populates="game")


class Vote(Base):
    __tablename__ = "votes"

    id = Column(Integer, primary_key=True, index=True)
    value = Column(Integer)
    game_id = Column(Integer, ForeignKey("games.id"))

    game = relationship("Game", back_populates="votes")
