from typing import List

from pydantic import BaseModel


class VoteBase(BaseModel):
    value: int
    game_id: int


class VoteCreate(VoteBase):
    pass


class Vote(VoteBase):
    id: int

    class Config:
        orm_mode = True


class GameBase(BaseModel):
    name: str
    min_players: int
    max_players: int
    bgg_link: str


class GameCreate(GameBase):
    pass


class Game(GameBase):
    id: int
    votes: List[Vote] = []

    class Config:
        orm_mode = True
