import React, { useEffect } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import AddGame from './components/AddGame';
import Results from './components/Results';
import VoteForm from './components/VoteForm';

// import Socket from './helper_functions/Socket';

import './App.css';

// const socket = new Socket();

const axios = require('axios').default;

axios.defaults.headers.post["Content-Type"] = "application/json";
axios.defaults.headers.put["Content-Type"] = "application/json";
axios.defaults.headers.patch["Content-Type"] = "application/json";

function App() {

  useEffect(() => {
    // socket.connect();

    return () => {
      // socket.disconnect();
    };
  },[]);

  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/newgame" render={props => <AddGame {...props} />} />
          <Route exact path="/results" render={props => <Results {...props} />} />
          <Route path="/" render={props => <VoteForm {...props} />} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
