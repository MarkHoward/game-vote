import React, { useState } from 'react';
import { Link } from "react-router-dom";
import swal from "sweetalert";

const axios = require('axios').default;

function AddGame(props) {
    const [state, setState] = useState({
        name: '',
        minPlayers: 0,
        maxPlayers: 0,
        bggLink: '',
    });

    const handleChange = event => setState({
        ...state,
        [event.target.name]: event.target.value,
    });

    const handleSubmit = async () => {
        if (state.name === '' || state.bggLink === '') {
            swal("Error", "Name and BGG Link are required", "error");
            return;
        }
        if (state.minPlayers < 1 || state.maxPlayers < 1 || state.minPlayers > state.maxPlayers) {
            swal("Error", "Invalid player count", "error");
            return;
        }
        try {
            swal("Sending...", "Attempting to create the game...", "info", {buttons: false});
            await axios.post('/api/games', JSON.stringify({
                name: state.name,
                min_players: state.minPlayers,
                max_players: state.maxPlayers,
                bgg_link: state.bggLink,
            }));
            swal("Success", "Game created!", "success");
            props.history.push('/');
        } catch (error) {
            swal("Error", error.response.data, "error");
        }
    };

    return (
        <div>
            <Link to='/'>Home</Link>
            <div className='grid-2'>
                <label htmlFor='name'>Game Name</label>
                <input type='text' name='name' id='name' placeholder='game name' onChange={handleChange} />
                <label htmlFor='minPlayers'>Minimum Players</label>
                <input type='number' name='minPlayers' id='minPlayers' onChange={handleChange} />
                <label htmlFor='maxPlayers'>Maximum Players</label>
                <input type='number' name='maxPlayers' id='maxPlayers' onChange={handleChange} />
                <label htmlFor='bggLink'>Board Game Geek Link</label>
                <input type='text' name='bggLink' id='bggLink' placeholder='BGG Link' onChange={handleChange} />
            </div>
            <button onClick={handleSubmit}>Submit</button>
        </div>
    );
}

export default AddGame;
