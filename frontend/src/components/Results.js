import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import swal from "sweetalert";

const axios = require('axios').default;

function Results(props) {
    const [state, setState] = useState({
        results: [],
    });

    useEffect(() => {
        async function fetchData() {
            const response = await axios.get('/api/games');
            setState({
                ...state,
                results: response.data.sort(compare),
            });
        }
        fetchData();
    },[]);

    const handleClear = async () => {
        try {
            swal("Sending...", "Attempting to erase votes...", "info", {buttons: false});
            await axios.delete('/api/votes');
            swal("Success", "Votes erased!", "success");
            props.history.push('/');
        } catch (error) {
            swal("Error", error.response.data, "error");
        }
    };

    function compare(game1, game2) {
        game1.voteTotal = game1.votes.map(vote => vote.value)
                                     .reduce((total, current) => total + current, 0);
        game2.voteTotal = game2.votes.map(vote => vote.value)
                                     .reduce((total, current) => total + current, 0);
        if (game1.voteTotal < game2.voteTotal) return 1;
        if (game2.voteTotal < game1.voteTotal) return -1;

        return 0;
    }

    return (
        <div>
            <Link to='/'>Home</Link>
            {state.results.map(game => <p key={game.id}>{game.name} - {game.voteTotal}</p>)}
            <button onClick={handleClear}>Clear Results</button>
        </div>
    );
}

export default Results;