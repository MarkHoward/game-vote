import React, { useEffect, useState } from 'react';
import { Link } from "react-router-dom";
import swal from "sweetalert";

const axios = require('axios').default;

function VoteForm(props) {
    const [state, setState] = useState({
        games: [],
        firstVote: 0,
        secondVote: 0,
        thirdVote: 0,
        downVote: 0,
        playerCount: 0,
        filteredGames: [],
    });

    useEffect(() => {
        async function fetchData() {
            const response = await axios.get('/api/games');
            let filteredGames = response.data;
            if (state.playerCount > 0) {
                filteredGames = filteredGames.filter((game) => {
                    return game.min_players <= state.playerCount && game.max_players >= state.playerCount;
                });
            }
            setState({
                ...state,
                games: response.data,
                filteredGames
            });
        }
        fetchData();
    },[]);

    const handleChange = event => setState({
        ...state,
        [event.target.name]: event.target.value,
    });

    const handleFilter = event => {
        let playerCount = event.target.value;
        let filteredGames = state.games;
        if (playerCount > 0) {
            filteredGames = filteredGames.filter((game) =>{
                return game.min_players <= playerCount && game.max_players >= playerCount;
            });
        }
        setState({
            ...state,
            playerCount,
            filteredGames,
        });
    };

    const handleSubmit = async () => {
        if (state.firstVote === 0 || state.secondVote === 0 || state.thirdVote === 0 || state.downVote === 0) {
            swal("Error", "You must select a game for each category", "error");
            return;
        }
        if (
            state.firstVote === state.secondVote ||
            state.firstVote === state.thirdVote ||
            state.firstVote === state.downVote ||
            state.secondVote === state.thirdVote ||
            state.secondVote === state.downVote ||
            state.thirdVote === state.downVote
            ) {
                swal("Error", "You cannot vote for the same game in multiple categories", "error");
                return;
            }
        try {
            swal("Sending...", "Attempting to send your votes...", "info", {buttons: false});
            await axios.post('/api/votes', JSON.stringify([
                {game_id: state.firstVote, value: 3},
                {game_id: state.secondVote, value: 2},
                {game_id: state.thirdVote, value: 1},
                {game_id: state.downVote, value: -3}
            ]));
            swal("Success", "Votes received!", "success");
            props.history.push('/results');
        } catch (error) {
            swal("Error", error.response.data, "error");
        }
    };

    return (
        <div>
            <Link to='/newgame'>Add Game</Link>
            <br />
            <Link to='/results'>Current Results</Link>
            <br />
            <label htmlFor='playerCount'>Player Count</label>
            <input type='number' name='playerCount' id='playerCount' onChange={handleFilter} />
            <div onChange={handleChange}>
                <p>Most want to play (worth 3 points)</p>
                {state.filteredGames.map(game => {
                    return <label key={`1-${game.id}`}>
                                <input type='radio' value={game.id} name='firstVote' />
                                <a href={game.bgg_link}>{game.name}</a>
                           </label>;
                })}
            </div>
            <div onChange={handleChange}>
                <p>Second most want to play (worth 2 points)</p>
                {state.filteredGames.map(game => {
                    return <label key={`2-${game.id}`}>
                                <input type='radio' value={game.id} name='secondVote' />
                                <a href={game.bgg_link}>{game.name}</a>
                           </label>;
                })}
            </div>
            <div onChange={handleChange}>
                <p>Third most want to play (worth 1 point)</p>
                {state.filteredGames.map(game => {
                    return <label key={`3-${game.id}`}>
                                <input type='radio' value={game.id} name='thirdVote' />
                                <a href={game.bgg_link}>{game.name}</a>
                           </label>;
                })}
            </div>
            <div onChange={handleChange}>
                <p>Do NOT want to play (worth -3 points)</p>
                {state.filteredGames.map(game => {
                    return <label key={`down-${game.id}`}>
                                <input type='radio' value={game.id} name='downVote' />
                                <a href={game.bgg_link}>{game.name}</a>
                           </label>;
                })}
            </div>
            <button onClick={handleSubmit}>Submit</button>
        </div>
    );
}

export default VoteForm;